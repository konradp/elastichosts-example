#!/bin/bash
OUTDIR=ansible/files

function die() {
  echo $@
  exit 1
}

# Prerequisites
for TOOL in ansible-vault ssh-keygen; do
  if ! command -v $TOOL &>/dev/null; then
    die This requires $TOOL tool to be present
  fi
done

# Generate SSH key pair
echo This will re-generate SSH keys and re-encrypt them.
echo Note: Existing files will be overwritten.
echo Note: Remember the Vault password entered below.
echo
yes y | ssh-keygen -f $OUTDIR/ssh_cloud_key -N "" -q
echo Key pair generated. Encrypting private key...

ansible-vault encrypt $OUTDIR/ssh_cloud_key
echo "Private (encypted) and public keys generated"
echo - $OUTDIR/ssh_cloud_key
echo - $OUTDIR/ssh_cloud_key.pub
echo
cat <<"EOF"
TODO:
 - Add and commit both above keys in git
 - Add below GitLab CI/CD variables:
   - VAULT_PASS: The password provided above
   - EHUSER:     ElasticHosts API user UUID
   - EHPASS:     ElasticHosts API user password
EOF

# Ansible deployment
These Ansible playbooks are best used by a CI/CD pipeline, but these instructions how to use them manually if needed.

# Prerequisites
This requires a `.vault` file to be present in the dir you have found this `README` file. The `.vault` file should contain the Ansible Vault password used to encrypt the SSH private key. See the steps in the main README file for generating this password.  
To run the playbooks, Ansible is required.

# Run
Run the playbook
```
ansible-playbook main.yml
```
